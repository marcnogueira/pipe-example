# Aplicação de Exemplo para a PIPE

## Configurando uma nova aplicação

  rails new nome-da-aplicacao -d postgresql -T -B

  //pg gem with postgres app

  gem install pg -- --with-pg-config=/Applications/Postgres.app/Contents/Versions/9.4/bin/pg_config

## Assuntos

* Ajustando Gemfile + Bundler
* GIT - Comandos básicos
* MVC
* Introdução a testes unitários
